'use strict'

window.wensn = {
  canvas: document.querySelector('canvas'),
  config: {
    type: 'line',
    options: {
      responsive: true,
      normalized: true,
      parsing: false,
      scales: {
        x: {
          type: 'time',
          time: {
            unit: 'hour',
            displayFormats: { hour: "d'.' LLL'.' HH'h'" },
          },
        },
        y: {
          min: 30, max: 100,
          ticks: {
            callback: value => value + ' dBA',
          },
          grid: {
            color: ctx => gradient(ctx.tick.value),
          },
        },
      },
      plugins: {
        legend: { display: false },
        title: {
          display: true,
          text: 'Schalldruckpegel (grün: leise, rot: laut, 60 dBA: Gesprächslautstärke)'
        },
        tooltip: {
          callbacks: {
            label: ctx => ctx.parsed.y.toFixed(2) + ' dBA'
          },
        },
        decimation: {
          enabled: true, algorithm: 'lttb',
        },
      },
    },
  },
  fns: {},
}
wensn.chart = new Chart(wensn.canvas, wensn.config)

wensn.process = function(rawData, cmd) {
  wensn.rawData = rawData
  wensn.processedData = []

  // init the command states
  const states = {}
  for (const cmd of wensn.cmds)
    states[cmd.name] = wensn.fns[cmd.name]?.initState?.()

  // convert raw data to data points then run the commands over them
  rawData.pop() // remove last element (is always null)
  outer: for (let line of rawData) {
    line = line.split(' ')
    let x = Date.parse(line[0]) // directly parse to epoch
    let y = +line[1]

    if (isNaN(x) || isNaN(y)) {
      console.warn('invalid data point skipped', {x, y})
      continue
    }

    // conditionally call commands
    for (const cmd of wensn.cmds) {
      // call command if exists
      const f = wensn.fns[cmd.name]
      const result = f && f(x, y, states[cmd.name], ...cmd.args)

      // if result is null skip the data point
      if (result === null) continue outer

      // if result is undefined use original data point
      if ('object' === typeof result) ({x, y} = result)
    }

    x = +x // convert DateTime to timestamp
    wensn.processedData.push({ x, y })
  }

  wensn.config.data = {
    datasets: [{
      data: wensn.processedData,
      borderColor: '#0000',
      backgroundColor: ctx => gradient(+ctx.raw?.y),
    }],
  }
  wensn.chart.update()
}

wensn.load = function() {
  const [ pat, cmd ] = location.hash.substr(1).split(':')
  wensn.cmds = parseCmds(cmd)

  // todo read stripes
  fetch('data.php?pat=' + (pat || '*'))
    .then(res => res.json())
    .then(json => wensn.process(json))
}

window.addEventListener('hashchange', wensn.load)
wensn.load()

// Syntax: <fn>(<arg>*,)*;
// Very simple and primitive parser, example: x();y(1,2)
function parseCmds(cmds) {
  const parsedCmds = []
  for (const cmd of (cmds || '').split(';')) {
    const matches = /(\w+)\(([^)]*)\)/.exec(cmd)
    if (matches) {
      parsedCmds.push({
        name: matches[1],
        args: matches[2].split(','),
      })
    }
    else {
      console.error(cmd, 'not parsed as a command, ignored')
      continue
    }
  }
  return parsedCmds
}

function gradient(y) {
  const high = 80
  const low = 20
  // 240 is hue from 180 down to 0 then from 360 down to 300
  const factor = 240 / (high - low)

  let hue = (high - y) * factor
  if (hue > 150) hue = 180
  if (hue < 0 && hue > -60) hue += 360
  if (hue < 0) hue = 300
  return `hsl(${hue}deg, 100%, 40%)`
}
