[<?

// Poduce JSON array [line, line, ..., line, null ]
// All log lines are strings, let JS parse the lines

header('Content-Type: application/json');
if (!isset($_GET['dbg'])) ini_set('display_errors', 0);

$pat = isset($_GET['pat']) ? $_GET['pat'] : '*';
foreach (glob("logs/$pat.log") as $filename) {
  if (!preg_match('/^logs\/[0-9]{10}\.log$/', $filename)) continue;

  $file = @fopen($filename, 'r');
  if (!$file) continue;
  while (false !== ($line = fgets($file)))
    echo "\"", trim($line), "\",\n"; // append comma
  fclose($file);
}

// null is a hack to simplify comma-handling because JSON is
// strict and doesn't allow a comma at the end

?>null]
