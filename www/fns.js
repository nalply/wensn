'use strict'

const nullFunction = () => void 0
const fns = wensn.fns

// return null if beyond limits, use NaN for no limit
fns.maxmin = (x, y, _, max, min) =>
  y > max || y < min ? null : {x, y}

// n is the number of data points to average over
fns.avg = function(x, y, state, n) {
  state.avg += y
  switch (state.phase++) {
    default: return null
    case n - 1: {
      y = state.avg / n
      state.avg = state.phase = 0
      return { x, y }
    }
  }
}

fns.avg.initState = function() {
  return { avg: 0, phase: 0 }
}
